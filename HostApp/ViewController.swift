//
//  ViewController.swift
//  HostApp
//
//  Created by Marco Siino on 21/01/2020.
//  Copyright © 2020 Marco Siino. All rights reserved.
//

import UIKit
import TheBinaryFramework
import AwesomeDependency

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let b = TheBinaryFramework()
        print(b.binaryMethod(str: "Lol"))
        print(b.useAwesomeDependency(str: "Asèd"))
        
        let c = AwesomeDependency()
        print(c.awesomeMethod(str: "From App", doubleAwesome: true))
    }


}

